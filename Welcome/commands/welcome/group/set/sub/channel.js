const config = require("../../../../../../config.json")
const localDb = config.paths.localDb
const fs = require("fs")
const { ActionRowBuilder, ButtonBuilder, ButtonStyle } = require("discord.js")
const { normalEmbed } = require("../../../../../../assets/Class/Embed")

exports.execute = async (bot, interaction, args) => {
 const options = args._hoistedOptions
 const newChannel = options[0].value
 const channel = await interaction.guild.channels.fetch(newChannel)

 if (channel.type != 0) return await interaction.reply({ content: "Le salon sellectioné n'est pas un salon textuel, veuillez en choisir un autre." })
 let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
 db = await JSON.parse(db)
 const guildDb = db[interaction.guild.id]

 guildDb.welcome.channel = newChannel

 fs.writeFileSync(localDb, JSON.stringify(db, null, 2))

 await interaction.reply(`Le salon de welcome a été changé en <#${newChannel}>`).catch(err => { console.log(err) })
}