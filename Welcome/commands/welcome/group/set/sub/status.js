const config = require("../../../../../../config.json")
const localDb = config.paths.localDb
const fs = require("fs")
const { ActionRowBuilder, ButtonBuilder, ButtonStyle } = require("discord.js")
const { normalEmbed } = require("../../../../../../assets/Class/Embed")

exports.execute = async (bot, interaction, args) => {
 const options = args._hoistedOptions
 const status = options[0].value

 let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
 db = await JSON.parse(db)
 const guildDb = db[interaction.guild.id]

 guildDb.welcome.status = status

 fs.writeFileSync(localDb, JSON.stringify(db, null, 2))

 await interaction.reply(`Le module a bien été \`${status === true ? "activé" : "désactivé"}\``).catch(err => { console.log(err) })
}