const config = require("../../../../../../config.json")
const localDb = config.paths.localDb
const fs = require("fs")
const { welcomeEmbed } = require("../../../../../../assets/Class/Embed")

exports.execute = async (bot, interaction, args) => {
 const options = args._hoistedOptions
 let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
 db = await JSON.parse(db)
 const guildDb = db[interaction.guild.id]

 const newMessage = {
  titre: null,
  description: null,
  image: null,
  datecreate: false,
  datejoin: false
 }

 options.forEach(option => {
  option.name === "image" ? newMessage[option.name] = option.attachment.attachment : newMessage[option.name] = option.value
 });


 guildDb.welcome.message = newMessage

 fs.writeFileSync(localDb, JSON.stringify(db, null, 2))

 await interaction.reply({ content: "Le message viens d'être modifié, voici a quoi il va ressembler :", embeds: [welcomeEmbed(interaction.member, interaction.guild, guildDb.welcome.message)], ephemeral: true }).catch(err => { console.log(err) })
}