/*
File disposition if group : ./group/groupname/subname.js
File disposition if just sub : ./sub/subname.js
*/
const { SlashCommandBuilder, PermissionsBitField, Options } = require("discord.js")
const config = require("../../../config.json")

module.exports = {
   name: "welcome",
   data: new SlashCommandBuilder()
      .setName("welcome")
      .setDescription("utilise le module de welcome du bot")
      .setDefaultMemberPermissions(PermissionsBitField.Flags.Administrator) // null or PermissionsBitField
      .setDMPermission(false) // false or true
      .addSubcommandGroup(group => group
         .setName("set")
         .setDescription("Change les propriétés du welcome")
         .addSubcommand(sub => sub
            .setName("channel")
            .setDescription("Change le salon de welcome")
            .addChannelOption(opt => opt
               .setName("channel")
               .setDescription("Le nouveau salon de welcome")
               .setRequired(true)
            )
         )
         .addSubcommand(sub => sub
            .setName("status")
            .setDescription("Change le status du module")
            .addBooleanOption(opt => opt
               .setName("status")
               .setDescription("Change le status du module")
               .setRequired(true)
            )
         )
         .addSubcommand(sub => sub
            .setName("message")
            .setDescription("change le message de bienvenue")
            .addStringOption(opt => opt
               .setName("titre")
               .setDescription("Change le titre du message")
               .setRequired(true)
            )
            .addStringOption(opt => opt
               .setName("description")
               .setDescription("Change la description du message")
               .setRequired(true)
            )
            .addAttachmentOption(opt => opt
               .setName("image")
               .setDescription("Change l'image du message.")
            )
            .addBooleanOption(opt => opt
               .setName("datecreate")
               .setDescription("Active / Desactive l'affichage de la date de création du compte de l'utilisateur")
            )
            .addBooleanOption(opt => opt
               .setName("datejoin")
               .setDescription("Active / Desactive l'affichage de la date de rejoint dans le serveur de l'utilisateur")
            )
         )
      )
   ,

   execute: async (bot, interaction, args) => {
      const group = args._group ? args._group : false
      const subcommand = args._subcommand ? args._subcommand : false

      const { execute } = require(group ? "./group/" + group + "/sub/" + subcommand : "./sub/" + subcommand)
      await execute(bot, interaction, args)
   }
}

