const fs = require("fs")
const config = require("../../config.json")
const localDb = config.paths.localDb
const { welcomeEmbed } = require("../../assets/Class/Embed")

module.exports = {
 name: "guildMemberAdd",
 once: false,

 async execute(client, member) {
  let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
  db = await JSON.parse(db)
  const guildDb = db[member.guild.id]
  const welcome = guildDb.welcome

  if (welcome.status != true) return
  const channel = await member.guild.channels.fetch(welcome.channel)
  if (!channel) return

  await channel.send({ embeds: [welcomeEmbed(member, member.guild, welcome.message)] }).catch(err => { console.log(err) })
 }
}