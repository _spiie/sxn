const { sleep } = require("../../assets/sleep")
const fs = require("fs")
const config = require("../../config.json")
const localDb = config.paths.localDb

module.exports = {
 name: "messageCreate",
 once: false,

 async execute(client, message) {
  const guild = message.guild

  let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
  db = await JSON.parse(db)
  const guildDb = db[guild.id]

  if (message.channel.id != guildDb.autoPurge.channel) return
  sleep(5000)
  try {
   await message.delete().catch(err => { })
  } catch (err) { }
 }
}