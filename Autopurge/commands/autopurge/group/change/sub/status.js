const fs = require("fs")
const config = require("../../../../../../config.json")
const localDb = config.paths.localDb

exports.execute = async (bot, interaction, args) => {
 const options = args._hoistedOptions
 const status = options[0].value

 let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
 db = await JSON.parse(db)
 const guildDb = db[interaction.guild.id]

 guildDb["autoPurge"].status = status

 fs.writeFileSync(localDb, JSON.stringify(db, null, 2))

 await interaction.reply({ content: `L'autopurge a été ${status ? "`activé`" : "`désactivé`"}.`, ephemeral: true }).catch(err => { })
}