/*
File disposition if group : ./group/groupname/subname.js
File disposition if just sub : ./sub/subname.js
*/
const { SlashCommandBuilder, PermissionsBitField } = require("discord.js")

module.exports = {
   name: "autopurge",
   data: new SlashCommandBuilder()
      .setName("autopurge")
      .setDescription("Use autopurge module")
      .setDefaultMemberPermissions(PermissionsBitField.Flags.Administrator) // null or PermissionsBitField
      .setDMPermission(false) // false or true
      .addSubcommandGroup(group => group
         .setName("change")
         .setDescription("Change parameters for autopurge module")
         .addSubcommand(sub => sub
            .setName("channel")
            .setDescription("Change the autopurge channel")
            .addChannelOption(option => option
               .setName('channel')
               .setDescription("Change le channel de l'autopurge")
               .setRequired(true)
            )
         )
         .addSubcommand(sub => sub
            .setName("status")
            .setDescription("Change the autopurge status")
            .addBooleanOption(option => option
               .setName('status')
               .setDescription("Change le status de l'autopurge")
               .setRequired(true)
            )
         )
      ),

   execute: async (bot, interaction, args) => {
      const group = args._group ? args._group : null
      const subcommand = args._subcommand ? args._subcommand : null

      const { execute } = require(group ? `./group/${group}/sub/${subcommand}` : `./sub/${subcommand}`)
      await execute(bot, interaction, args)
   }
}