const Discord = require('discord.js');

const commandLoader = require("./Client/loaders/commandsLoader")
const eventsLoader = require("./Client/loaders/eventsLoader")

const config = require('./config.json');

const client = new Discord.Client({ intents: 3276799 });

client.commands = new Discord.Collection()
commandLoader(client)
eventsLoader(client)

client.login(config.token);