const fs = require("fs")
module.exports = async (client) => {
  const sections = fs.readdirSync("./").filter(file => !file.includes(".") && !file.includes("node_modules") && !file.includes("assets"))
  sections.forEach(section => {
    try {
      const commands = fs.readdirSync(`./${section}/events`).filter(file => file.includes('.js'))
      commands.forEach(eventFile => {
        const event = require(`../../${section}/events/${eventFile}`)
        if (event.once) {
          client.once(event.name, event.execute.bind(null, client))
        } else {
          client.on(event.name, event.execute.bind(null, client))
        }
        console.log(`Events : ${event.name} loaded !`)
      })
    } catch (err) { }
  })
}
