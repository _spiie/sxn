const fs = require("fs")

module.exports = async (client) => {
  const sections = fs.readdirSync("./").filter(file => !file.includes(".") && !file.includes("node_modules"))
  sections.forEach(section => {
    try {
      const commandDirs = fs.readdirSync(`./${section}/commands`)
      commandDirs.forEach(commandDir => {
        const command = require(`../../${section}/commands/${commandDir}/index.js`)
        if ('execute' in command && "data" in command) {
          client.commands.set(command.name, command)
          console.log(`Command : ${command.name} loaded !`)
        } else {
          console.log(`WARNING : The commande a ./${section}/commands/${commandFile} n'a pas toutes les propriétés requises ("data" & "execute")`)
        }
      })
    } catch (err) {
      console.log(err);
    }
  })
}