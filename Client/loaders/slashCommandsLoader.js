const Discord = require("discord.js")
const { REST } = require("@discordjs/rest")
const { Routes } = require("discord.js")
const config = require("../../config.json")

module.exports = async (client) => {
  let commands = [];

  client.commands.forEach(async command => {
    commands.push(command.data)
  });

  const rest = new REST({ version: "10" }).setToken(config.token)
  await rest.put(Routes.applicationCommands(client.user.id), { body: commands })
  console.log("Slash Loaded !")
}