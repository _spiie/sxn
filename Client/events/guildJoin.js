const fs = require("fs")
const config = require("../../config.json")
const paths = config.paths

module.exports = {
  name: "guildCreate",
  once: false,

  async execute(client, guild) {
    const db = fs.readFileSync(paths.localDb, { encoding: "utf-8", flag: "r" })
    const dbJSON = await JSON.parse(db)
    dbJSON[guild.id] = {
      customisation: {
        embeds: {
          colors: {
            normal: "#7e81be",
            red: "#be7e7e",
            yellow: "#beb97e",
            green: "#81be7e",
            white: "#FFFFF"
          }
        }
      },
      welcome: {
        status: false,
        channel: null,
        message: {
          titre: "Lorem Impsum",
          description: "Lorem Impsum",
          image: null,
          datecreate: false,
          datejoin: false
        }
      },
      reactionsRoles: {
        num: 0,
        created: {},
        sended: {}
      },
      autoPurge: {
        status: false,
        channel: null
      },
      moderation: {
        commands: {
          warn: {
            set: {
              permission: "",
              rolePermissions: []
            }
          }
        },
        banned: {
          members: {}
        },
        warn: {
          num: 0,
          warns: {}
        }
      },
      logs: {
        warn: ""
      }
    }

    fs.writeFileSync('./localDb.json', JSON.stringify(dbJSON, null, 2))
  }
}