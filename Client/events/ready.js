const slashCommandsLoader = require("../loaders/slashCommandsLoader")

module.exports = {
  name: "ready",
  once: false,

  async execute(client) {
    await slashCommandsLoader(client)
    client.user.setPresence({ activities: [{ name: 'Bot officiel de AnimeFR !' }], status: 'online' });
    console.log(`${client.user.tag} est en ligne !`)
  }
}