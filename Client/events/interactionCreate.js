const Discord = require("discord.js")

module.exports = {
  name: "interactionCreate",
  once: false,

  async execute(client, interaction) {
    if (interaction.type === Discord.InteractionType.ApplicationCommand) {
      await client.commands.get(interaction.commandName).execute(client, interaction, interaction.options)
    }
  }
}