const fs = require("fs")
const config = require("../../config.json")
const paths = config.paths

module.exports = {
  name: "guildDelete",
  once: false,

  async execute(client, guild) {
    const db = fs.readFileSync(paths.localDb, { encoding: "utf-8", flag: "r" })
    const dbJSON = await JSON.parse(db)
    delete dbJSON[guild.id]

    fs.writeFileSync('./localDb.json', JSON.stringify(dbJSON, null, 2))
  }
}