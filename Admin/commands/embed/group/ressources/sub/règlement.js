const { ActionRowBuilder, ButtonBuilder, ButtonStyle } = require("discord.js")
const { normalEmbed } = require("../../../../../../assets/Class/Embed")

exports.execute = async (bot, interaction, args) => {

 const embed = normalEmbed(interaction.guild.id, require("../../../../../../localDb.json"))
  .setTitle("REGLEMENT")

 return await interaction.reply({ embeds: [embed] })
}