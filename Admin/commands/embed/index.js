/*
File disposition if group : ./group/groupname/subname.js
File disposition if just sub : ./sub/subname.js
*/

const { SlashCommandBuilder, PermissionsBitField } = require("discord.js")

module.exports = {
  name: "embed",
  data: new SlashCommandBuilder()
    .setName("embed")
    .setDescription("Permet d'utiliser la fonction d'embed du bot")
    .setDefaultMemberPermissions(PermissionsBitField.Flags.Administrator) // null or PermissionsBitField
    .setDMPermission(false) // false or true
    .addSubcommandGroup(group => group
      .setName("ressources")
      .setDescription("Liste des embeds pour le serveur NoName")
      .addSubcommand(sub => sub
        .setName("règlement")
        .setDescription("Affiche le règlement du serveur.")
      )),

  execute: async (bot, interaction, args) => {
    const group = args._group ? args._group : null
    const subcommand = args._subcommand ? args._subcommand : null

    const { execute } = require(group ? `./group/${group}/sub/${subcommand}` : `./sub/${subcommand}`)
    await execute(bot, interaction, args)
  }
}