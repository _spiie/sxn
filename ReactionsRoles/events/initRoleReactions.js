const config = require("../../config.json")
const localDb = config.paths.localDb
const fs = require("fs")

module.exports = {
 name: "ready",
 once: false,

 async execute(client) {
  let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
  db = await JSON.parse(db)
  client.guilds.cache.forEach(async guild => {
   const guildDb = db[guild.id]
   if (guildDb) {
    for (const [key, value] of Object.entries(guildDb.reactionsRoles.sended)) {
     const channel = guild.channels.cache.get(value.channel)
     if (channel) {
      const message = await channel.messages.fetch(key).catch(err => { console.log(err) })
      if (message) {
       const collector = message.createMessageComponentCollector()
       collector.on('collect', async (i) => {
        value.roles.forEach(async role => {
         if (!i.values.includes(role.roleId)) await i.member.roles.remove(role.roleId)
        })
        i.values.forEach(async role => {
         if (!i.member.roles.cache.get(role.roleId)) await i.member.roles.add(role).catch(err => { console.log(err) })
        })
        i.deferUpdate().catch(err => { console.log(err) })
       })
      }
     }
    }
   }
  });

 }
}