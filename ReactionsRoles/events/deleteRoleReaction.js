const config = require("../../config.json")
const localDb = config.paths.localDb
const fs = require("fs")

module.exports = {
 name: "messageDelete",
 once: false,

 async execute(client, message) {
  let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
  db = await JSON.parse(db)
  const guildDb = db[message.guild.id]

  if (guildDb.reactionsRoles.sended[message.id]) {
   delete guildDb.reactionsRoles.sended[message.id]
   fs.writeFileSync('./localDb.json', JSON.stringify(db, null, 2))
  }

 }
}