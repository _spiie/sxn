const config = require("../../../../config.json")
const localDb = config.paths.localDb
const fs = require("fs")

exports.execute = async (bot, interaction, args) => {
 const options = args._hoistedOptions

 let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
 db = await JSON.parse(db)
 const guildDb = db[interaction.guild.id]

 const reactionRole = guildDb.reactionsRoles.created[options[0].value]
 if (!reactionRole) return await interaction.reply({ content: "Le reaction role demandé n'existe pas.", ephemeral: true }).catch(err => { console.log(err) })

 delete guildDb.reactionsRoles.created[options[0].value]

 fs.writeFileSync(localDb, JSON.stringify(db, null, 2))

 return await interaction.reply({ content: `Le patern de reaction role n°\`${options[0].value}\` a bien été supprimé !`, ephemeral: true }).catch(err => { console.log(err) })
}