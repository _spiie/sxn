const { ButtonBuilder, ActionRowBuilder, ButtonStyle, ModalBuilder, TextInputBuilder, TextInputStyle } = require("discord.js")
const config = require("../../../../config.json")
const localDb = config.paths.localDb
const fs = require("fs")
const { normalEmbed } = require("../../../../assets/Class/Embed")

exports.execute = async (bot, interaction, args) => {
  let title = "Choisissez vos rôles."
  let once = false
  let rolesListes = []

  const createEmbed = (once, rolesListes) => {
    const embed = normalEmbed(interaction.guild.id)
      .setTitle("Test")
      .setDescription(`**Titre :** \`${title}\`\n**Once :** \`${once}\`\n**Roles** : ${rolesListes.length === 0 ? "\`Aucuns roles pour le momment...\`" : `\n>>> ${rolesListes.map(role => `<@&${role.roleId}>\n`).join('')}`}`)

    return embed
  }

  const menu = new ActionRowBuilder()
    .addComponents(
      new ButtonBuilder()
        .setCustomId("changeTitle")
        .setLabel("📝 Changer le titre")
        .setStyle(ButtonStyle.Primary),
      new ButtonBuilder()
        .setCustomId("addRole")
        .setLabel("➕ Ajouter un rôle")
        .setStyle(ButtonStyle.Primary),
      new ButtonBuilder()
        .setCustomId("delRole")
        .setLabel("❌ Retirer un rôle")
        .setStyle(ButtonStyle.Danger),
      new ButtonBuilder()
        .setCustomId("changeType")
        .setLabel("💢 Once / Multiples")
        .setStyle(ButtonStyle.Primary),
      new ButtonBuilder()
        .setCustomId("validate")
        .setLabel("✅ Valider")
        .setStyle(ButtonStyle.Success)
    )

  const message = await interaction.reply({ embeds: [createEmbed(once, rolesListes)], components: [menu], ephemeral: true }).catch(err => { console.log(err) })
  const filter = (i) => i.user.id === interaction.member.id
  const messageCollector = await message.createMessageComponentCollector(filter)

  await messageCollector.on('collect', async (messageI) => {
    const customId = messageI.customId
    if (customId === "changeTitle") {
      const titleModal = new ModalBuilder()
        .setCustomId("titleModal")
        .setTitle("Définir un nouveau titre.")
        .addComponents(
          new ActionRowBuilder()
            .addComponents(
              new TextInputBuilder()
                .setCustomId("title")
                .setLabel("Le nouveau titre")
                .setStyle(TextInputStyle.Short)
            )
        )
      await messageI.showModal(titleModal).catch(err => { console.log(err) })
      const titleModalFilter = modalI => messageI.user.id === modalI.user.id
      const titleSubmitted = await messageI.awaitModalSubmit({ filter: titleModalFilter, time: 60000 }).catch(err => {
        return null
      })

      if (titleSubmitted) {
        const newName = titleSubmitted.fields.getTextInputValue("title")
        title = newName
        await interaction.editReply({ embeds: [createEmbed(once, rolesListes)], components: [menu], ephemeral: true })
        return await titleSubmitted.deferUpdate().catch(err => { console.log(err) })
      }

    } else if (customId === "addRole") {
      const roleModal = new ModalBuilder()
        .setCustomId("addRoleModal")
        .setTitle("Ajouter un rôle.")
        .addComponents(
          new ActionRowBuilder()
            .addComponents(
              new TextInputBuilder()
                .setCustomId("role")
                .setLabel("L'id du rôle")
                .setStyle(TextInputStyle.Short)
            ),
          new ActionRowBuilder()
            .addComponents(
              new TextInputBuilder()
                .setCustomId("title")
                .setLabel("Le titre affiché.")
                .setStyle(TextInputStyle.Short)
            )
        )
      await messageI.showModal(roleModal)
      const roleModalFilter = modalI => messageI.user.id === modalI.user.id
      const roleModalSubmitted = await messageI.awaitModalSubmit({ filter: roleModalFilter, time: 60000 }).catch(err => {
        return null
      })

      if (roleModalSubmitted) {
        const newRole = roleModalSubmitted.fields.getTextInputValue("role")
        const titleRole = roleModalSubmitted.fields.getTextInputValue('title')
        if (!await interaction.guild.roles.fetch(newRole)) {
          return await roleModalSubmitted.reply({ content: "L'id utilisé ne correspond a aucuns rôles, veuillez ré essayer.", ephemeral: true })
        } else {
          rolesListes.push({ roleId: newRole, title: titleRole })
          await interaction.editReply({ embeds: [createEmbed(once, rolesListes)], components: [menu], ephemeral: true }).catch(err => { console.log(err) })
        }
        return await roleModalSubmitted.deferUpdate().catch(err => { console.log(err) })
      }

    } else if (customId === "delRole") {
      const roleModal = new ModalBuilder()
        .setCustomId("delRoleModal")
        .setTitle("Ajouter un rôle.")
        .addComponents(
          new ActionRowBuilder()
            .addComponents(
              new TextInputBuilder()
                .setCustomId("role")
                .setLabel("L'id du rôle")
                .setStyle(TextInputStyle.Short)
            )
        )
      await messageI.showModal(roleModal)
      const roleModalFilter = modalI => messageI.user.id === modalI.user.id
      const roleModalSubmitted = await messageI.awaitModalSubmit({ filter: roleModalFilter, time: 60000 }).catch(err => {
        return null
      })

      if (roleModalSubmitted) {
        const delRole = roleModalSubmitted.fields.getTextInputValue("role")
        if (!delRole in rolesListes) {
          return await roleModalSubmitted.reply({ content: "Le role n'est pas dans la liste.", ephemeral: true }).catch(err => { console.log(err) })
        } else {
          for (let i = 0; i < rolesListes.length; i++) {
            if (rolesListes[i] === delRole) {
              rolesListes.splice(i, 1)
              break
            }
          }
          console.log(rolesListes)
          await interaction.editReply({ embeds: [createEmbed(once, rolesListes)], components: [menu], ephemeral: true }).catch(err => { console.log(err) })
        }
        return await roleModalSubmitted.deferUpdate().catch(err => { console.log(err) })
      }
    } else if (customId === "changeType") {
      once = once === true ? false : true
      await interaction.editReply({ embeds: [createEmbed(once, rolesListes)], components: [menu], ephemeral: true }).catch(err => { console.log(err) })
    } else if (customId === "validate") {
      let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
      db = await JSON.parse(db)
      const guildDb = db[interaction.guild.id]

      if (guildDb.reactionsRoles.created[title]) {
        await interaction.editReply({ content: "Le nom choisis est déjà utilisé, veuillez le changer.", embeds: [createEmbed(once, rolesListes)], components: [menu], ephemeral: true }).catch(err => { console.log(err) })
      } else if (rolesListes.length === 0) {
        await interaction.editReply({ content: "Votre reaction role ne contiens pas de rôles, veuillez en ajouter au moins un.", embeds: [createEmbed(once, rolesListes)], components: [menu], ephemeral: true }).catch(err => { console.log(err) })
      } else {
        guildDb.reactionsRoles.num++
        guildDb.reactionsRoles.created[guildDb.reactionsRoles.num] = {
          name: title,
          roles: rolesListes,
          once
        }
        fs.writeFileSync(localDb, JSON.stringify(db, null, 2))
        await interaction.editReply({ content: "L'autorole a bien été ajouté !", embeds: [], components: [] }).catch(err => { console.log(err) })
      }

    }

    await messageI.deferUpdate().catch(err => { console.log(err) })
  })
}