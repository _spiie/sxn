const config = require("../../../../config.json")
const localDb = config.paths.localDb
const { normalEmbed } = require("../../../../assets/Class/Embed")
const fs = require("fs")

exports.execute = async (bot, interaction, args) => {
 const options = args._hoistedOptions
 let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
 db = await JSON.parse(db)
 const guildDb = db[interaction.guild.id]

 const embed = normalEmbed(interaction.guild.id)
  .setTitle('Reactions Roles - Liste')

 let description = ""
 Object.entries(guildDb.reactionsRoles.created).forEach((value) => {
  description += `\n> \`${value[0]}\` : __${value[1].name}__\n`
 })
 embed.setDescription(description)

 await interaction.reply({ embeds: [embed], ephemeral: true }).catch(err => { console.log(err) })

}