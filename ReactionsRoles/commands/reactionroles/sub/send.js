const config = require("../../../../config.json")
const localDb = config.paths.localDb
const fs = require("fs")
const { normalEmbed } = require("../../../../assets/Class/Embed")
const { ActionRowBuilder, StringSelectMenuBuilder } = require("discord.js")

exports.execute = async (bot, interaction, args) => {
  const options = args._hoistedOptions
  let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
  db = await JSON.parse(db)
  const guildDb = db[interaction.guild.id]

  const reactionRole = guildDb.reactionsRoles.created[options[0].value]
  if (!reactionRole) return await interaction.reply({ content: "Le reaction role demandé n'existe pas.", ephemeral: true }).catch(err => { console.log(err) })

  const embed = normalEmbed(interaction.guild.id)
    .setTitle(reactionRole.name)
    .setDescription(`${reactionRole.roles.map(role => `➜ <@&${role.roleId}>\n`).join('')}`)

  const menu = new ActionRowBuilder()
    .addComponents(
      new StringSelectMenuBuilder()
        .setCustomId("roleSelect")
        .setPlaceholder("Obtenir des rôles.")
    )

  reactionRole.once ? menu.components[0].setMaxValues(1) : menu.components[0].setMaxValues(reactionRole.roles.length)
  reactionRole.roles.forEach(role => {
    const guildRole = interaction.guild.roles.cache.get(role.roleId)
    menu.components[0].addOptions({
      label: role.title,
      description: `@${guildRole.name}`,
      value: role.roleId
    })
  });

  const message = await interaction.channel.send({ embeds: [embed], components: [menu] }).catch(err => { console.log(err) })
  const collector = message.createMessageComponentCollector()
  collector.on('collect', async (i) => {
    reactionRole.roles.forEach(async role => {
      if (!i.values.includes(role.roleId)) await i.member.roles.remove(role.roleId)
    })
    console.log(i.values);
    i.values.forEach(async role => {
      if (!i.member.roles.cache.get(role.roleId)) await i.member.roles.add(role)
    })
    i.deferUpdate().catch(err => { console.log(err) })
  })

  await interaction.reply({ content: "Autorôle envoyé !", ephemeral: true }).catch(err => { console.log(err) })
  guildDb.reactionsRoles.sended[message.id] = {
    channel: message.channel.id,
    roles: reactionRole.roles,
    once: reactionRole.once
  }

  fs.writeFileSync(localDb, JSON.stringify(db, null, 2))
}
