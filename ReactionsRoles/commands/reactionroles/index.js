/*
File disposition if group : ./group/groupname/subname.js
File disposition if just sub : ./sub/subname.js
*/
const { SlashCommandBuilder, PermissionsBitField } = require("discord.js")

const config = require("../../../config.json")
const localDb = config.paths.localDb
const fs = require("fs")

module.exports = {
 name: "reactionrole",
 data: new SlashCommandBuilder()
  .setName("reactionrole")
  .setDescription("Reactions Roles")
  .setDefaultMemberPermissions(PermissionsBitField.Flags.Administrator) // null or PermissionsBitField
  .setDMPermission(false) // false or true
  .addSubcommand(sub => sub
   .setName("create")
   .setDescription("Crée un nouveau reaction role")
  )
  .addSubcommand(sub => sub
   .setName("send")
   .setDescription("Envois un reaction role")
   .addIntegerOption(opt => opt
    .setName('id')
    .setDescription("L'id de l'autorôle.")
    .setRequired(true)
   )
  )
  .addSubcommand(sub => sub
   .setName("list")
   .setDescription("Envois la list des reaction")
  )
  .addSubcommand(sub => sub
   .setName("delete")
   .setDescription("Supprime un patern de role reaction.")
   .addIntegerOption(opt => opt
    .setName("id")
    .setDescription("Id du patern")
    .setRequired(true)
   )
  ),

 execute: async (bot, interaction, args) => {
  const group = args._group ? args._group : null
  const subcommand = args._subcommand ? args._subcommand : null

  const { execute } = require(group ? "./group/" + group + "/sub/" + subcommand : "./sub/" + subcommand)
  await execute(bot, interaction, args)
 }
}