const config = require("../../config.json")
const localDb = config.paths.localDb
const fs = require("fs")

module.exports = {
 name: "ready",
 once: false,

 async execute(client) {
  setInterval(async () => {
   let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
   db = await JSON.parse(db)

   client.guilds.cache.forEach(async guild => {
    // guild = await client.guilds.fetch(guild)
    const guildDb = db[guild.id]
    if (guildDb) {
     for (const [key, value] of Object.entries(guildDb.moderation.banned.members)) {
      const today = Date.now()
      if (value - today <= 0) {
       await guild.bans.remove(key, "end tempban").catch(err => { console.log(err) })
       delete guildDb.moderation.banned.members[key]

       fs.writeFileSync(localDb, JSON.stringify(db, null, 2))
      }
     }
    };
   });
  }, 15000);
 }
}