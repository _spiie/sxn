/*
File disposition if group : ./group/groupname/subname.js
File disposition if just sub : ./sub/subname.js
*/
const { SlashCommandBuilder, PermissionsBitField } = require("discord.js")

module.exports = {
 name: "unban",
 data: new SlashCommandBuilder()
  .setName("unban")
  .setDescription("Unban un membre du serveur.")
  .setDefaultMemberPermissions(PermissionsBitField.Flags.BanMembers) // null or PermissionsBitField
  .setDMPermission(false) // false or true
  .addUserOption(opt => opt
   .setName("user")
   .setDescription("L'utilisateur à unban")
   .setRequired(true)
  )
  .addStringOption(opt => opt
   .setName("raison")
   .setDescription("La raison de ce unban")
  ),

 execute: async (bot, interaction, args) => {
  const options = args._hoistedOptions
  const reason = options[2] ? options[2].value : null

  const member = await interaction.guild.bans.fetch(options[0].value).catch(err => { console.log(err) })
  if (!member) return await interaction.reply({ content: "Cet utilisateur n'est pas bannis.", ephemeral: true }).catch(err => { console.log(err) })
  try {
   await interaction.bans.remove(member.id, reason)
  } catch (error) {
   return await interaction.reply({ content: "Je n'ai pas la permission de unban cet utilisateur.", ephemeral: true }).catch(err => { console.log(err) })
  }
  return await interaction.reply({ content: `Le membre <@${member.id}> a été unban du serveur.`, ephemeral: true }).catch(err => { console.log(err) })
 }
}