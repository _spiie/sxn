/*
File disposition if group : ./group/groupname/subname.js
File disposition if just sub : ./sub/subname.js
*/
const { SlashCommandBuilder, PermissionsBitField } = require("discord.js")

module.exports = {
 name: "kick",
 data: new SlashCommandBuilder()
  .setName("kick")
  .setDescription("Kick un membre du serveur.")
  .setDefaultMemberPermissions(PermissionsBitField.Flags.KickMembers) // null or PermissionsBitField
  .setDMPermission(false) // false or true
  .addUserOption(opt => opt
   .setName("user")
   .setDescription("L'utilisateur à kick")
   .setRequired(true)
  )
  .addStringOption(opt => opt
   .setName("raison")
   .setDescription("La raison de ce kick")
  ),

 execute: async (bot, interaction, args) => {
  const options = args._hoistedOptions
  const reason = options[2] ? options[2].value : null

  const member = await interaction.guild.members.fetch(options[0].value).catch(err => { console.log(err) })
  if (!member) return await interaction.reply({ content: "Cet utilisateur n'est pas dans la guilde.", ephemeral: true })
  if (!member.id === interaction.member.id) return await interaction.reply({ content: "Vous ne pouvez pas vous kick vous meme.", ephemeral: true }).catch(err => { console.log(err) })
  if (await interaction.guild.ownerId === interaction.member.id || interaction.guild.roles.comparePositions(interaction.member.roles.highest, member.roles.highest) <= 0) return await interaction.reply({ content: "Vous ne pouvez pas kick kick cet utilisateur.", ephemeral: true }).catch(err => { console.log(err) })

  try {
   await member.kick(reason)
  } catch (error) {
   return await interaction.reply({ content: "Je n'ai pas la permission de kick cet utilisateur.", ephemeral: true }).catch(err => { console.log(err) })
  }
  return await interaction.reply({ content: `Le membre <@${member.id}> a été kick du serveur.`, ephemeral: true }).catch(err => { console.log(err) })
 }
}