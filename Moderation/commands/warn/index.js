/*
File disposition if group : ./group/groupname/subname.js
File disposition if just sub : ./sub/subname.js
*/
const { SlashCommandBuilder, PermissionsBitField, Options } = require("discord.js")
const config = require("../../../config.json")

module.exports = {
   name: "warn",
   data: new SlashCommandBuilder()
      .setName("warn")
      .setDescription("utilise le module de warn du bot")
      .setDefaultMemberPermissions(null) // null or PermissionsBitField
      .setDMPermission(false) // false or true
      .addSubcommand(sub => sub
         .setName("add")
         .setDescription("Ajoute un warn à quelqu'un")
         .addUserOption(user => user
            .setName("user")
            .setDescription("Le membre a bannir")
            .setRequired(true)
         )
         .addStringOption(opt => opt
            .setName("raison")
            .setDescription("Ajoute une raison au warn")
            .setRequired(true)
         )
      )
      .addSubcommand(sub => sub
         .setName('remove')
         .setDescription("Enleve un warn à quelqu'un")
         .addIntegerOption(opt => opt
            .setName("id")
            .setDescription("L'id du warn")
            .setRequired(true)
         )
      ),

   execute: async (bot, interaction, args) => {
      const group = args._group ? args._group : false
      console.log(group)
      const subcommand = args._subcommand ? args._subcommand : false

      const { execute } = require(group ? "./group/" + group + "/sub/" + subcommand : "./sub/" + subcommand)
      await execute(bot, interaction, args)
   }
}