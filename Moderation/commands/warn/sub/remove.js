const config = require("../../../../config.json")
const localDb = config.paths.localDb
const { greenEmbed } = require('../../../../assets/Class/Embed')
const fs = require("fs")

exports.execute = async (bot, interaction, args) => {
 const options = args._hoistedOptions
 const guild = interaction.guild
 const moderator = interaction.member
 const warnId = parseInt(options[0].value)

 let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
 db = await JSON.parse(db)
 const guildDb = db[guild.id]
 const guildWarns = guildDb.moderation.warn

 const deletedWarn = guildWarns.warns[warnId.toString()]
 if (!deletedWarn) return await interaction.reply({ content: "Ce warn n'éxiste pas.", ephemeral: true })

 delete guildWarns.warns[warnId.toString()]

 fs.writeFileSync(localDb, JSON.stringify(db, null, 2))
 await interaction.reply({ content: `Le warn numérot : #${warnId} a été supprimé !` })

 try {
  const logEmbed = greenEmbed(guild.id)
   .setAuthor({ name: "Suppression de warn..." })
   .addFields(
    { name: "Commands", value: `>>> **Action** : DelWarn\n**N°Warn** : ${warnId}\n**Moderator** : <@${moderator.id}> (\`${moderator.id}\`)`, inline: true },
    { name: "Warn", value: `**Utilisateur** : <@${deletedWarn.user}> (\`${deletedWarn.user}\`)\n**Modérateur :** <@${deletedWarn.moderator}> (\`${deletedWarn.moderator}\`)\n**Raison :** ${deletedWarn.raison}\n**Date :** <t:${deletedWarn.timestamp}><t:${deletedWarn.timestamp}:R>` }
   )
   .setTimestamp()
   .setFooter({ text: `Del - Warn#${warnId}` })
  await interaction.guild.channels.cache.get(guildDb.logs.warn).send({ embeds: [logEmbed] })
 } catch (err) { console.log(err) }
}