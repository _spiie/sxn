const config = require("../../../../config.json")
const localDb = config.paths.localDb
const { yellowEmbed } = require('../../../../assets/Class/Embed')
const fs = require("fs")

exports.execute = async (bot, interaction, args) => {
 const options = args._hoistedOptions
 const guild = interaction.guild
 const moderator = interaction.member
 const user = await interaction.guild.members.fetch(options[0].value).user
 const raison = options[1].value
 let timestamp = Math.floor(Date.now() / 1000)

 let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
 db = await JSON.parse(db)
 const guildDb = db[guild.id]
 const guildWarns = guildDb.moderation.warn
 guildWarns.num += 1
 const warnId = guildWarns.num

 guildWarns.warns[warnId.toString()] = {
  id: warnId,
  user: user.id,
  moderator: moderator.id,
  reason: raison,
  timestamp
 }

 fs.writeFileSync(localDb, JSON.stringify(db, null, 2))
 await interaction.reply({ content: `<@${user.id}> was warned !` })

 try {
  const logEmbed = yellowEmbed(guild.id)
   .setAuthor({ name: user.username + user.tag, iconURL: user.avatarURL() })
   .setDescription(`>>> **Action** : Warn\n**Utilisateur** : <@${user.id}> (\`${user.id}\`)\n**Modérateur :** <@${moderator.id}> (\`${moderator.id}\`)\n**Raison :** ${raison}\n**Date :** <t:${timestamp}><t:${timestamp}:R>`)
   .setTimestamp()
   .setFooter({ text: `Warn #${warnId}` })
  await interaction.guild.channels.cache.get(guildDb.logs.warn).send({ embeds: [logEmbed] })
 } catch (err) { console.log(err); }
}