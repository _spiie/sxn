/*
File disposition if group : ./group/groupname/subname.js
File disposition if just sub : ./sub/subname.js
*/
const { SlashCommandBuilder, PermissionsBitField } = require("discord.js")
const fs = require("fs")
const config = require("../../../config.json")
const localDb = config.paths.localDb
const ems = require("enhanced-ms");

module.exports = {
 name: "ban",
 data: new SlashCommandBuilder()
  .setName("ban")
  .setDescription("Ban un membre du serveur.")
  .setDefaultMemberPermissions(PermissionsBitField.Flags.BanMembers) // null or PermissionsBitField
  .setDMPermission(false) // false or true
  .addUserOption(opt => opt
   .setName("user")
   .setDescription("L'utilisateur à bannir")
   .setRequired(true)
  )
  .addStringOption(opt => opt
   .setName("temps")
   .setDescription('d = day h = hours m = minutes s = secons ')
  )
  .addStringOption(opt => opt
   .setName("raison")
   .setDescription("La raison de ce ban")
  ),

 execute: async (bot, interaction, args) => {
  const options = args._hoistedOptions
  let temps = options[1] && options[1].name === "temps" ? options[1].value : null
  let reason = null
  if (options[1] && options[1].name === "raison") {
   reason = options[1].value
  } else if (options[2] && options[2].name === "raison") {
   reason = options[2].value
  }
  if (temps) {
   temps = ems(temps);
   if (!temps) return interaction.reply({ content: "Veuillez ecrire un temps valide. Exemple: 1d/1h/1m/1s", ephemeral: true }).catch(err => { console.log(err) })
   temps = Date.now() + temps
  }

  let member = await interaction.guild.members.fetch(options[0].user.id).catch(err => { console.log(err) })
  if (member) {
   if (!member) return await interaction.reply({ content: "Cet utilisateur n'est pas dans la guilde.", ephemeral: true })
   if (!member.id === interaction.member.id) return await interaction.reply({ content: "Vous ne pouvez pas vous ban vous meme.", ephemeral: true })
   if (await interaction.guild.ownerId === interaction.member.id || interaction.guild.roles.comparePositions(interaction.member.roles.highest, member.roles.highest) <= 0) return await interaction.reply({ content: "Vous ne pouvez pas ban ban cet utilisateur.", ephemeral: true }).catch(err => { console.log(err) })
  } else member = options[0].user

  try {
   await interaction.guild.bans.create(member, { reason: reason }).catch(err => { console.log(err) })
  } catch (error) {
   return await interaction.reply({ content: "Je n'ai pas la permission de ban cet utilisateur.", ephemeral: true }).catch(err => { console.log(err) })
  }
  await interaction.reply({ content: `Le membre <@${member.id}> a été ban du serveur.`, ephemeral: true }).catch(err => { console.log(err) })
  if (temps) {
   let db = fs.readFileSync(localDb, { encoding: "utf-8", flag: "r" })
   db = await JSON.parse(db)
   const guildDb = db[interaction.guild.id]

   guildDb.moderation.banned.members[member.id] = temps

   fs.writeFileSync(localDb, JSON.stringify(db, null, 2))
  }
 }
}