/*
File disposition if group : ./group/groupname/subname.js
File disposition if just sub : ./sub/subname.js
*/
const { SlashCommandBuilder, PermissionsBitField } = require("discord.js")
const ems = require("enhanced-ms");

module.exports = {
 name: "mute",
 data: new SlashCommandBuilder()
  .setName("mute")
  .setDescription("Mute un membre du serveur.")
  .setDefaultMemberPermissions(PermissionsBitField.Flags.ManageMessages) // null or PermissionsBitField
  .setDMPermission(false) // false or true
  .addUserOption(opt => opt
   .setName("user")
   .setDescription("L'utilisateur à mute")
   .setRequired(true)
  )
  .addStringOption(opt => opt
   .setName("temps")
   .setDescription('d = day h = hours m = minutes s = secons ')
  )
  .addStringOption(opt => opt
   .setName("raison")
   .setDescription("La raison de ce mute")
  ),

 execute: async (bot, interaction, args) => {
  const options = args._hoistedOptions
  let temps = options[1] && options[1].name === "temps" ? options[1].value : "28d"
  let reason = null
  if (options[1] && options[1].name === "raison") {
   reason = options[1].value
  } else if (options[2] && options[2].name === "raison") {
   reason = options[2].value
  }

  temps = ems(temps);
  if (!temps) return interaction.reply({ content: "Veuillez ecrire un temps valide. Exemple: 1d/1h/1m/1s", ephemeral: true }).catch(err => { console.log(err) })

  const member = await interaction.guild.members.fetch(options[0].value).catch(err => { console.log(err) })
  if (!member) return await interaction.reply({ content: "Cet utilisateur n'est pas dans la guilde.", ephemeral: true })
  if (!member.id === interaction.member.id) return await interaction.reply({ content: "Vous ne pouvez pas vous mute vous meme.", ephemeral: true })
  if (await interaction.guild.ownerId != interaction.member.id && interaction.guild.roles.comparePositions(interaction.member.roles.highest, member.roles.highest) <= 0) return await interaction.reply({ content: "Vous ne pouvez pas mute cet utilisateur.", ephemeral: true }).catch(err => { console.log(err) })

  try {
   await member.timeout(temps, reason)
  } catch (error) {
   return await interaction.reply({ content: "Je n'ai pas la permission de timeout cet utilisateur.", ephemeral: true }).catch(err => { console.log(err) })
  }
  return await interaction.reply({ content: `Le membre <@${member.id}> a été timeout.`, ephemeral: true }).catch(err => { console.log(err) })
 }
}