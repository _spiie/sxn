/*
File disposition if group : ./group/groupname/subname.js
File disposition if just sub : ./sub/subname.js
*/
const { SlashCommandBuilder, PermissionsBitField } = require("discord.js")
const ems = require("enhanced-ms");

module.exports = {
 name: "unmute",
 data: new SlashCommandBuilder()
  .setName("unmute")
  .setDescription("UnMute un membre du serveur.")
  .setDefaultMemberPermissions(PermissionsBitField.Flags.ManageMessages) // null or PermissionsBitField
  .setDMPermission(false) // false or true
  .addUserOption(opt => opt
   .setName("user")
   .setDescription("L'utilisateur à mute")
   .setRequired(true)
  )
  .addStringOption(opt => opt
   .setName("raison")
   .setDescription("La raison de ce mute")
  ),

 execute: async (bot, interaction, args) => {
  const options = args._hoistedOptions
  console.log(options);
  let reason = options[1] ? options[1].value : null

  const member = await interaction.guild.members.fetch(options[0].value).catch(err => { console.log(err) })
  if (!member) return await interaction.reply({ content: "Cet utilisateur n'est pas dans la guilde.", ephemeral: true }).catch(err => { console.log(err) })
  if (!member.id === interaction.member.id) return await interaction.reply({ content: "Vous ne pouvez pas vous unmute vous meme.", ephemeral: true }).catch(err => { console.log(err) })
  if (await interaction.guild.ownerId != interaction.member.id && interaction.guild.roles.comparePositions(interaction.member.roles.highest, member.roles.highest) <= 0) return await interaction.reply({ content: "Vous ne pouvez pas unmute cet utilisateur.", ephemeral: true }).catch(err => { console.log(err) })

  try {
   await member.timeout(null, reason)
  } catch (error) {
   return await interaction.reply({ content: "Je n'ai pas la permission de unmute cet utilisateur.", ephemeral: true })
  }
  return await interaction.reply({ content: `Le membre <@${member.id}> a été unmute.`, ephemeral: true }).catch(err => { console.log(err) })
 }
}