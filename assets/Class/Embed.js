const { EmbedBuilder } = require("discord.js")
const config = require("../../config.json")
const localDb = config.paths.localDb
const fs = require("fs")

const readDb = async () => {
 const db = fs.readFileSync(paths.localDb, { encoding: "utf-8", flag: "r" })
 return dbJSON = await JSON.parse(db)
}

exports.normalEmbed = (guildId) => {

 const embed = new EmbedBuilder()
  .setColor(readDb()[guildId].customisation.embeds.colors.normal)

 return embed
}

exports.redEmbed = (guildId) => {
 const embed = new EmbedBuilder()
  .setColor(readDb()[guildId].customisation.embeds.colors.red)

 return embed
}

exports.yellowEmbed = (guildId) => {
 const embed = new EmbedBuilder()
  .setColor(readDb()[guildId].customisation.embeds.colors.yellow)

 return embed
}

exports.greenEmbed = (guildId) => {
 const embed = new EmbedBuilder()
  .setColor(readDb()[guildId].customisation.embeds.colors.green)

 return embed
}


exports.whiteEmbed = (guildId) => {
 const embed = new EmbedBuilder()
  .setColor(readDb()[guildId].customisation.embeds.colors.white)

 return embed
}

exports.welcomeEmbed = (user, guild, welcomeData) => {
 let title = welcomeData.titre
 let description = welcomeData.description
 const image = welcomeData.image
 const datecreate = welcomeData.datecreate
 const datejoin = welcomeData.datejoin

 description = description.replace("{user}", `<@${user.id}>`).replace("{guild}", guild.name).replace("{userTag}", user.user.tag).replace('{username}', user.user.username).replace("{memberCount}", guild.memberCount)
 title = title.replace("{user}", `<@${user.id}>`).replace("{guild}", guild.name).replace("{userTag}", user.user.tag).replace('{username}', user.user.username).replace("{memberCount}", guild.memberCount)


 const embed = this.normalEmbed(guild.id)
  .setAuthor({ name: user.user.tag, iconURL: user.user.avatarURL() })
  .setTitle(title)
  .setDescription(description)

 image ? embed.setImage(image) : null
 datecreate ? embed.addFields({ name: "Date de Création", value: `<t:${user.user.createdTimestamp}> <t:${user.user.createdTimestamp}:R>`, inline: true }) : null
 datejoin ? embed.addFields({ name: "Date de Création", value: `<t:${user.joinedTimestamp}> <t:${user.joinedTimestamp}:R>`, inline: true }) : null

 return embed
}